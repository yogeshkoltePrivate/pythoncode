__author__ = 'yogeshkolte'
import os
julie_scores = []
julie_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter5/julie.txt"
james_scores=[]
james_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter5/james.txt"
mikey_scores = []
mikey_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter5/mikey.txt"
sarah_scores = []
sarah_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter5/sarah.txt"

def readIntoList(file_path, lst):
    with open(file_path, 'r') as scores:
        tmp = scores.read().split(",")
        for val in tmp:
            lst.append(sanitize(val.strip()))


def sanitize(time_string):
    splitter=''
    if ':' in time_string:
       splitter = ':'
    elif "-" in time_string:
        splitter='-'
    else:
        return (time_string)

    (mins, secs) = time_string.split(splitter)

    return (mins + "." + secs)



readIntoList(julie_file_path, julie_scores)
readIntoList(james_file_path, james_scores)
readIntoList(mikey_file_path, mikey_scores)
readIntoList(sarah_file_path, sarah_scores)

print("Julie", " - ", julie_scores)
print("James", " - ", james_scores)
print("Mikey", " - ", mikey_scores)
print("Sarah", " - ", sarah_scores)

julie_scores.sort()
# james_scores.sort()
# mikey_scores.sort()
# sarah_scores.sort()

julie_scores = set(julie_scores)
# james_scores = set(james_scores)
# mikey_scores = set(mikey_scores)
# sarah_scores = set(sarah_scores)
#
#
#
print("Julie", " - ", list(julie_scores)[0:3])
# print("James", " - ", james_scores[0:3])
# print("Mikey", " - ", mikey_scores[0:3])
# print("Sarah", " - ", sarah_scores[0:3])
