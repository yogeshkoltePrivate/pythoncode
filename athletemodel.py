__author__ = 'yogeshkolte'
import pickle
from inheritanceTest import AthleteList
julie_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/julie2.txt"

james_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/james2.txt"

mikey_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/mikey2.txt"

sarah_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/sarah2.txt"
def get_coach_data(filename):
    try:
        with open(filename) as f:
            data = f.readline()
        templ = data.strip().split(',')
        return(AthleteList(templ.pop(0), templ.pop(0), templ))
    except IOError as ioerr:
        print('File error: ' + str(ioerr))
        return(None)

def put_to_store(file_list):
    all_athletes = {}

    for file in file_list:
        athlete = get_coach_data(file)
        all_athletes[athlete.name] = athlete

    try:
        with open("allathletes.obj", 'wb') as f:
            pickle.dump(all_athletes, f)
    except IOError as ioerror:
        print("File IO Error " + str(ioerror))

    return (all_athletes)

def get_from_store():
    all_athletes = {}
    try:
        with open("allathletes.obj", 'r') as f:
            all_athletes = pickle.load(f)

    except IOError as ioerror:
        print("File IO Error " + str(ioerror))

    return (all_athletes)

the_files = [sarah_file_path, mikey_file_path, james_file_path, julie_file_path]

data = put_to_store(the_files)

print(data)