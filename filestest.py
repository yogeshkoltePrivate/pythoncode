__author__ = 'yogesh'
from sys import argv

script, from_file, to_file = argv
print "Copying file from %s to %s " %(from_file, to_file)
in_file = open(from_file)
in_data = in_file.read()
print "The input file is %d bytes long." % len(in_data)
print "Does the output file exist? %r" % exit(to_file)

print "Return to continue CTRL-C to abort."
raw_input()

out_file = open(to_file, 'w')
out_file.write(in_data)

print "Done"
out_file.close()
in_file.close()

