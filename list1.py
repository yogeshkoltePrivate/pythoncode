__author__ = 'yogeshkolte'
julie_scores = []
julie_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/julie2.txt"
james_scores=[]
james_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/james2.txt"
mikey_scores = []
mikey_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/mikey2.txt"
sarah_scores = []
sarah_file_path = "/Users/yogeshkolte/pythonCode/hfpy_code/chapter6/sarah2.txt"

class Athlete:
    def __init__(self, a_name, a_dob=None, a_times=[]):
        self.name = a_name
        self.dob = a_dob
        self.times = a_times

    def sanitize(time_string):
        splitter=''
        if ':' in time_string:
           splitter = ':'
        elif "-" in time_string:
            splitter='-'
        else:
            return (time_string)

        (mins, secs) = time_string.split(splitter)

        return (mins + "." + secs)

    def top3(self):
        return str(sorted(set(sanitize(val) for val in self.times))[0:3])

    def addTime(self, time):
        self.times.append(time)

    def addTimes(self, times):
        self.times.extend(times)



s = Athlete('Sarah Sweeny', '2000-01-01', ['2:28', '2:40', '1:56', '0:55'])
type(s)


def get_coach_data(filename):
    try:
        with open(filename, 'r') as f:
            data = f.readline()
            temp1 = data.strip().split(',')
            return Athlete(temp1.pop(0),
                    temp1.pop(0),
                    temp1
            )

    except IOError as error:
        print('File Error : ' + str(error))
        return (None)


def sanitize(time_string):
    splitter=''
    if ':' in time_string:
       splitter = ':'
    elif "-" in time_string:
        splitter='-'
    else:
        return (time_string)

    (mins, secs) = time_string.split(splitter)

    return (mins + "." + secs)

sarah = get_coach_data(sarah_file_path)
# sarah_data ={}
# sarah_data['name'] = sarah_scores.pop(0)
# sarah_data['dob'] = sarah_scores.pop(0)
# sarah_data['times'] = sarah_scores

print(sarah.name + "'s fastest scores " + str(sarah.top3()))